package test.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.min;

class Task {

    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Fields~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Field cities type ArrayList<City> save input info in class form
    * Field vNum type int save number of cities
    * Field graph type int[][] to save adjacency matrix for view transportation model
    * Field start type String save source city
    * Field finish type String save destination city*/
    private ArrayList<City> cities = new ArrayList<>();
    private int vNum;
    private int[][] graph;
    private String start;
    private String finish;
    private final int INF = 20000;
    private final int COUNT_OF_CHARS = 10;

    /*Constructor no arguments, call one method, witch call another methods*/
    Task() {
        scenario();
    }

    /*Method scenario call methods in right sequence test times*/
    private void scenario () {
        int test = inputQuantityTest();
        while (test > 0) {
            inputCity();
            inputSrcAndDst();
            test--;
        }
    }

    /*Method inputQuantityTest(), where user input quantity of "tests" - numbers of run some methods*/
    private int inputQuantityTest () {
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the quantity of cities: ");
            count = Integer.parseInt(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /*Method inputCity(), where user input all data according to condition, set fields, create object of class City,
    * and fill the fields of this class*/
    private void inputCity() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the quantity of cities: ");
            int count = Integer.parseInt(br.readLine());
            graph = new int[count][count];
            vNum = count;
            if (count > 0) {
                while (count > 0) {
                    City tmp = new City();
                    if (cities.isEmpty()) {
                        final int START_ID = 1;
                        tmp.setId(START_ID);
                    } else {
                        int cityId = cities.get(cities.size() - 1).getId() + 1;
                        if (cityId < tmp.getMAX_ID() && cityId > tmp.getMIN_ID()) tmp.setId(cityId);
                    }
                    System.out.println("Enter city's name: ");
                    String cityName = br.readLine();
                    if (!cityName.matches(".*\\d+.*") && !cityName.matches(".* +.*") && (cityName.length() <= COUNT_OF_CHARS)) {
                        tmp.setName(cityName);
                    }
                    System.out.println("Enter quantity of city's neighbors: ");
                    int numNeighbors = Integer.parseInt(br.readLine());
                    if (numNeighbors < tmp.getMAX_NUMS_NUMBERS_OF_NEIGHBORS() && numNeighbors > tmp.getMIN_NUMS_NUMBERS_OF_NEIGHBORS()) {
                        tmp.setNumsNeighbors(numNeighbors);
                    }
                    while (numNeighbors > 0) {
                        System.out.println("Enter id of nearby city and through <SPACE> transportation cost: ");
                        String cityNeighborsAndCost = br.readLine();
                        Pattern pattern = Pattern.compile("(\\d+) (\\d+)");
                        Matcher matcher = pattern.matcher(cityNeighborsAndCost);
                        while (matcher.find()) {
                            int cityNeighborsId = Integer.parseInt(matcher.group(1));
                            int cost = Integer.parseInt(matcher.group(2));
                            if ((cityNeighborsId < tmp.getMAX_ID()) && (cityNeighborsId > tmp.getMIN_ID()) && (cityNeighborsId != tmp.getId()) && (cost > 0)) {
                                tmp.getNeighborAndCost().put(cityNeighborsId, cost);
                                this.graph[tmp.getId() - 1][cityNeighborsId - 1] = cost;
                            }
                        }
                        numNeighbors--;
                    }
                    this.cities.add(tmp);
                    count--;
                }
            } else {
                throw new IllegalArgumentException("Number of cities less than zero");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*Method inputSrcAndDst() where user input quantity of paths and source city and destination city*/
    private String[] inputSrcAndDst () {
        String[] srcDst = new String[2];
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the quantity of paths: ");
            int count = Integer.parseInt(br.readLine());
            while (count > 0) {
                System.out.println("Enter the source city through dash(-) destination city: ");
                String cityName = br.readLine();
                Pattern pattern = Pattern.compile("(\\w+) - (\\w+)");
                Matcher matcher = pattern.matcher(cityName);
                while (matcher.find()) {
                    srcDst[0] = this.start = matcher.group(1);
                    srcDst[1] = this.finish = matcher.group(2);
                }
                count--;
                System.out.println("For cities: " + this.start + " and " + this.finish + "transportation cost equal: ");
                System.out.println(definePath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return srcDst;
    }

    /*Method floydWarshall() witch use algorithm Floyd-Warshall, that search costs and rearranges their in that order:
    * each element of array means distance between vertices of the graph, algorithm based
    * on splitting the process of searching for the shortest paths to phases.
    * Description of algorithm:
    * The shortest path from vertex i to vertex j, which is allowed to pass through vertices in addition {1, 2, ..., k},
    * coincides with the shortest path that is allowed to pass through the vertices of the set {1, 2, ..., k - 1}.
    * In this case, the value of d [i] [j] does not change when passing from the k-th to the k + 1-st phase.
    * If this "new" path is split by the vertex k into two halves, then each of these halves no longer enters the vertex k.
    * But then it turns out that the length of each of these halves was calculated even in the k-th phase or even earlier,
    * and it is enough to simply take the sum d [i] [k] + d [k] [j], and it will give the length "new" shortest path.*/
    private int[][] floydWarshall () {
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph.length; j++) {
                if (this.graph[i][j] == 0 && i != j) {
                    this.graph[i][j] = this.INF;
                }
            }
        }
        int[][] dist = new int [vNum][vNum];
        for (int i = 0; i < vNum; i++) System.arraycopy(graph[i], 0, dist[i], 0, vNum);
        for (int k = 0; k < vNum; k++)
            for (int i = 0; i < vNum; i++)
                for (int j = 0; j < vNum; j++)
                    if (dist[i][k] < INF && dist[k][j] < INF)
                    dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
        return dist;
    }

    /*Method definePath() witch search in list of cities and compare city name with city id*/
    private int definePath () {
        int startId = 0;
        int finishId = 0;
        for (City city : cities) {
            if (city.getName().compareToIgnoreCase(this.start) == 0) {
                startId = city.getId();
            } else if (city.getName().compareToIgnoreCase(this.finish) == 0) {
                finishId = city.getId();
            }
        }
        return floydWarshall()[startId - 1][finishId - 1];
    }
}
