package test.task3;

import lombok.Data;

import java.util.LinkedHashMap;

@Data
class City {
    private int id;
    private final int MIN_ID = 0;
    private final int MAX_ID = 10000;
    private String name;
    private int numsNeighbors;
    private final int MAX_NUMS_NUMBERS_OF_NEIGHBORS = 100;
    private final int MIN_NUMS_NUMBERS_OF_NEIGHBORS = 0;
    private LinkedHashMap<Integer, Integer> neighborAndCost = new LinkedHashMap<>();

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
