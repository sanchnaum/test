package test.task1_2;

import java.io.*;
import java.math.BigInteger;
import java.util.LinkedHashSet;

public class Tasks {



    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Task1~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /*Method countOfscopes() to input String from console, witch define quantity of entries returning method
     * scopesReq (int n), where argument is number of scopes */
    public int countOfscopes() {
        int count = 0;
        System.out.println("Enter the number of scopes: ");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            int n = Integer.parseInt(br.readLine());
            for (String s : scopesReq(n)) {
                System.out.println(s);
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /*Method scopesReq(int n) witch count right quantity of variants scopes position,
    * first of all set variants for first two number of this sequence, for next number of sequence,
    * set recurrent search right variant through recursive call this method*/
    private LinkedHashSet<String> scopesReq (int n) {
        LinkedHashSet<String> res = new LinkedHashSet<>();
        if (n > 0) {
            if (n == 1) {
                res.add("()");
            } else if (n == 2) {
                res.add("()()");
                res.add("(())");
            } else {
                for (String s : scopesReq(n - 1)) {
                    StringBuilder tmp = new StringBuilder();
                    res.add(tmp.append("(").append(s).append(")").toString());
                    StringBuilder tmp1 = new StringBuilder();
                    for (String s1 : scopesReq(1)) {
                        res.add(tmp1.append(s).append(s1).toString());
                    }
                    StringBuilder tmp2 = new StringBuilder();
                    for (String s1 : scopesReq(1)) {
                        res.add(tmp2.append(s1).append(s).toString());
                    }
                }
            }
        } else {
            throw new IllegalArgumentException("You enter number less than zero");
        }
        return res;
    }



    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Task3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /*Method sumOfFactorial (int n), receives number witch search, and converts to String, that call each char,
    * and parse to int, and add to result sum, argument int n is passed as an argument into method factorial (int n),
    * witch define search number*/
    public int sumOfFactorial (int n) {
        BigInteger number = factorial(n);
        String s = number.toString();
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            sum += Integer.parseInt(Character.toString(s.charAt(i)));
        }
        return sum;
    }

    /*Method factorial (int n), is passed as an argument number witch factorial is search, use type BigInteger,
    * as a variable where set searching number, because some numbers witch factorial is search much larger than
    * MAX_VALUE of Integer type, because need to find all digits of number*/
    private BigInteger factorial (int n) {
        BigInteger result = BigInteger.ONE;
        if (n > 0) {
            if (n == 0) {
                result = BigInteger.ONE;
            } else {
                for (int i = 1; i <= n; i++) {
                    result = result.multiply(BigInteger.valueOf(i));
                }
            }
        } else {
            throw new IllegalArgumentException("You enter number less than zero");
        }
        return result;
    }
}
